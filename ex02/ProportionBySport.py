# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    ProportionBySport.py                               :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 13:37:52 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 15:08:15 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import parent_import
import pandas as pd
from ex01.FileLoader import *


@log
def proportionBySport(data, year, sport, gender):
    gendered = data[
        (data.Sex == gender) &
        (data.Year == year)
    ]
    gendered = gendered.drop_duplicates("Name")
    filtered = gendered[gendered.Sport == sport]
    if gendered["Name"].count() == 0:
        raise ZeroDivisionError(f"There was no {gender} practicing "
                                f"{sport} in {year} Olympics")
    return filtered["Name"].count() / gendered["Name"].count()


if __name__ == "__main__":
    loader = FileLoader()
    data = loader.load("https://raw.githubusercontent.com/42-AI/bootcamp"
                       "_python/master/day04/resources/athlete_events.csv")
    print(proportionBySport(data, 2004, 'Tennis', 'F'))
