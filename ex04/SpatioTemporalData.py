# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    SpatioTemporalData.py                              :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 20:17:02 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 20:17:02 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#


import parent_import
import pandas as pd
from ex01.FileLoader import *


class SpatioTemporalData:
    def __init__(self, data):
        self.data = data.filter(["Year", "City"])

    def when(self, location):
        return list(self.data.loc[self.data.City == location].Year.unique())

    def where(self, date):
        return list(self.data.loc[self.data.Year == date].City.unique())


def test_SpatioTemporalData():
    loader = FileLoader()
    data = loader.load("https://raw.githubusercontent.com/42-AI/bootcamp"
                       "_python/master/day04/resources/athlete_events.csv")
    sp = SpatioTemporalData(data)
    print(sp.data)
    assert sp.where(1896) == ['Athina']
    assert sp.where(2016) == ['Rio de Janeiro']
    assert sp.when('Athina') == [2004, 1906, 1896]
    assert sp.when('Paris') == [1900, 1924]
