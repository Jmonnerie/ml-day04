# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    HowManyMedals.py                                   :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 18:42:00 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 18:42:01 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import parent_import
import pandas as pd
from pprint import pprint
from ex01.FileLoader import *


def countMedals(obj):
    ret = obj\
            .value_counts()\
            .to_frame()["Medal"]\
            .to_dict()
    pattern = {
        "Gold": 0,
        "Silver": 0,
        "Bronze": 0
    }
    pattern.update(ret)
    pattern["G"] = pattern.pop("Gold")
    pattern["S"] = pattern.pop("Silver")
    pattern["B"] = pattern.pop("Bronze")
    return pattern


@log
def howManyMedals(data, name=None):
    dude = data[data.Name == name] if name is not None else data
    return dude.pivot_table(
            index="Year",
            values="Medal",
            aggfunc=countMedals
        ).to_dict()["Medal"]


if __name__ == "__main__":
    loader = FileLoader()
    data = loader.load("https://raw.githubusercontent.com/42-AI/bootcamp"
                       "_python/master/day04/resources/athlete_events.csv")
    pprint(howManyMedals(data, 'Kjetil Andr Aamodt'))
