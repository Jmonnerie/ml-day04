# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    FileLoader.py                                      :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 10:08:04 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 10:08:04 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import os
import sys
import time
import getpass
import pandas as pd


logg = open("./machine.log",  "w+")


def log(func):
    def wrap(*args,  **kwargs):
        username = getpass.getuser()

        time1 = time.time()
        ret = func(*args,  *kwargs)
        time2 = time.time()

        timer = (time2-time1) * 1000
        measure = "ms"
        if timer >= 1000:
            measure = "s"
            timer = timer / 1000
        logg.write('({})Running: {:13} [ exec-time = {:5.3f} {:2} ]\n'
                   .format(username,  func.__name__,  timer,  measure))
        logg.write("\tin  >\n" + str(args) + " " + str(kwargs) + "\n")
        logg.write("\tout >\n" + str(ret) + "\n")
        return ret
    return wrap


class FileLoader:
    @staticmethod
    @log
    def load(path):
        pdDF = pd.read_csv(path, sep=",", index_col=0)
        features = pdDF.columns
        height, width = len(pdDF), len(features)
        print(f"Loading dataset of dimensions {height} x {width}")
        return pdDF

    @staticmethod
    @log
    def display(df, n):
        print(df.head(n) if n >= 0 else df.tail(-n))


if __name__ == "__main__":
    fl = FileLoader()
    try:
        dataSet = fl.load("./mescouilles")
    except FileNotFoundError as e:
        print("FileNotFoundErrror: " + str(e))
        sys.exit()
    except ValueError as e:
        print("ValueError: " + str(e))
        sys.exit()
    print("============================== display 5\n")
    logg.write("============================== display 5\n")
    fl.display(dataSet, 5)
    logg.write(str(dataSet[:5]))
    print("============================== display -5\n")
    logg.write("============================== display -5\n")
    fl.display(dataSet, -5)
    logg.write(str(dataSet[-5:]))
    print("============================== display 0\n")
    logg.write("============================== display 0\n")
    fl.display(dataSet, 0)
    logg.write(str(dataSet[:0]))
    logg.close()
