# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    YoungestFellah.py                                  :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 12:47:30 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 12:47:55 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import pandas as pd
import numpy as np
from FileLoader import *


def youngestFellah(data, year):
    # male = data[(data.Year == year) & (data.Sex == "M")].Age.min()
    # female = data[(data.Year == year) & (data.Sex == "F")].Age.min()    RIP IN PEACE MON CODE
    # return {
    #     "f_age": male,
    #     "m_age": female
    # }
    return \
        data[data.Year == year]\
        .pivot_table(index="Sex", values=["Age"], aggfunc=np.min)\
        .to_dict()["Age"]


if __name__ == "__main__":
    loader = FileLoader()
    data = loader.load("https://raw.githubusercontent.com/42-AI/bootcamp"
                       "_python/master/day04/resources/athlete_events.csv")
    print(youngestFellah(data, 2004))
