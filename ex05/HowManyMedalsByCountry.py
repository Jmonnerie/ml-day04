# ****************************************************************************#
#                                                                             #
#                                                         :::      ::::::::   #
#    HowManyMedalsByCountry.py                          :+:      :+:    :+:   #
#                                                     +:+ +:+         +:+     #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+        #
#                                                 +#+#+#+#+#+   +#+           #
#    Created: 2020/05/30 22:49:11 by jojomoon          #+#    #+#             #
#    Updated: 2020/05/30 23:18:01 by jojomoon         ###   ########lyon.fr   #
#                                                                             #
# ****************************************************************************#

import parent_import
import pandas as pd
from pprint import pprint
from ex01.FileLoader import *
from ex03.HowManyMedals import *


@log
def howManyMedalsByCountry(data, country):
    dudes = data[data.Team == country]
    if len(dudes) == 0:
        return dict()
    dudes = dudes.drop_duplicates(
            subset=["Team", "City", "Event"],
            keep="last")
    return howManyMedals(dudes)


if __name__ == "__main__":
    loader = FileLoader()
    data = loader.load("https://raw.githubusercontent.com/42-AI/bootcamp"
                       "_python/master/day04/resources/athlete_events.csv")
    pprint(howManyMedalsByCountry(data, 'China'))
